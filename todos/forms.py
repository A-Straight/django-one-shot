from django.forms import ModelForm
from todos.models import TodoList, TodoItem


class CreateTodoList(ModelForm):
    class Meta:
        model = TodoList
        fields = [
            'name',
        ]


class EditTodoList(ModelForm):
    class Meta:
        model = TodoList
        fields = [
            'name',
        ]


class AddItemList(ModelForm):
    class Meta:
        model = TodoItem
        fields = [
            'task',
            'due_date',
            'is_completed',
            'list',
        ]


class EditItemList(ModelForm):
    class Meta:
        model = TodoItem
        fields = [
            'task',
            'due_date',
            'is_completed',
            'list',
        ]
