from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import CreateTodoList, EditTodoList, AddItemList, EditItemList

# Create your views here.


def todolist(request):
    todolist = TodoList.objects.all()
    context = {
        "todolist": todolist
    }
    return render(request, 'todos/list.html', context)


def todo_list_detail(request, id):
    show_list = get_object_or_404(TodoList, id=id)
    context = {
        'show_list': show_list
    }
    return render(request, 'todos/detail.html', context)


def todo_list_create(request):
    if request.method == "POST":
        form = CreateTodoList(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect('todo_list_detail', id=list.id)
    else:
        form = CreateTodoList()

    context = {
        "form": form
    }
    return render(request, 'todos/create.html', context)


def todo_list_update(request, id):
    post = get_object_or_404(TodoList, id=id)
    if request.method == 'POST':
        form = EditTodoList(request.POST, instance=post)
        if form.is_valid():
            item = form.save()
            return redirect('todo_list_detail', id=item.id)
    else:
        form = EditTodoList()

    context = {
        'form': form
    }
    return render(request, 'todos/edit.html', context)


def todo_list_delete(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        model_instance.delete()
        return redirect('todo_list_list')

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = AddItemList(request.POST)
        if form.is_valid():
            item = form.save()
            item.save()
            return redirect('todo_list_detail', id=item.list.id)
    else:
        form = AddItemList()

    context = {
        'form': form
    }
    return render(request, 'todos/additem.html', context)


def todo_item_update(request, id):
    post = get_object_or_404(TodoItem, id=id)
    if request.method == 'POST':
        form = EditItemList(request.POST, instance=post)
        if form.is_valid():
            item = form.save()
            item.save()
            return redirect('todo_list_detail', id=item.list.id)
    else:
        form = EditItemList(instance=post)

    context = {
        'form': form
    }
    return render(request, 'todos/edititem.html', context)
